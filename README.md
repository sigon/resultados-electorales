# resultados-electorales


Datos obtenidos del area de descargas del [Ministerio del Interior](http://www.infoelectoral.mir.es/infoelectoral/min/areaDescarga.html?method=inicio)

Estructura del nombre del fichero: **ELECCIONES**_**aaaa**_**mm**_**CPRO**.json

ELECCIONES = ['generales', 'municipales', 'europeas']

aaaa =  año

mm =  mes

CPRO = codigo provincia (09 = BURGOS)

Campos:

- year = 2016
- mes = 06
- CUSEC = CPRO + CMUN + CDIS + CSEC
- censo
- blancos
- nulos
- candidatura


https://sigon.gitlab.io/resultados-electorales/data/generales_2011_11_09.json

https://sigon.gitlab.io/resultados-electorales/data/generales_2015_12_09.json

https://sigon.gitlab.io/resultados-electorales/data/generales_2016_06_09.json


## LICENCIA

Origen de los datos: [Ministerio del Interior](https://datos.gob.es/avisolegal)
