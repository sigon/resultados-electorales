# this script import json files generated with R-elecciones and updates them
# so the party votes are grouped

import json
import os

dirname=os.path.dirname
cwd = os.getcwd()
RESULTADOS_ELECTORALES_DIR = dirname(dirname(os.getcwd()))
DATA_DIR = RESULTADOS_ELECTORALES_DIR + '/public/data'
FILE = DATA_DIR + '/generales_2016_06_09'

file_list = os.listdir(DATA_DIR)


def reorganize(cfile):

    with open(DATA_DIR+'/'+cfile+'.json') as f:
        data = json.load(f)
    newData = []
    i = 0
    for cusecData in data:
        newData.append({
            'cusec': cusecData['CUSEC'],
            'censo': cusecData['censo'],
            'blancos': cusecData['blancos'],
            'nulos': cusecData['nulos'],
            'candidatura': cusecData['candidatura'],
            'partidos': {}
        })

        for attribute, value in cusecData.items():
            if (attribute not in ['votos', 'CUSEC', 'censo', 'blancos','nulos','candidatura', 'year', 'mes']):
                newData[i]['partidos'][attribute] = value
        i += 1
    with open(DATA_DIR +'/'+ cfile +'_.json', 'w') as jsonfile:
        json.dump(newData, jsonfile)

for file in file_list:
    if ".json" in file:
        file = file.replace(".json", "");
        print(file)
        reorganize(file)
