# R elecciones

El script elecciones.R usa una libreria de R llamada [R-elecciones](https://r-elecciones.netlify.com/) para descargar resultados electorales.

Esos datos se pasan luego a json.

El script se puede usar directamente desde la consola de R-studio:

```
source("/home/sigon/Proyects/GitLab/resultados-electorales/scripts/R/elecciones.R")
```

## Instalar R-studio

Probado para Ubuntu 18.04.2

Instalamos R (en este caso mediante PPA)

```shell
$ sudo add-apt-repository ppa:marutter/rrutter
$ sudo apt-get update sudo
$ apt-get install r-base r-base-dev    
```

Instalamos R-studio:

https://www.rstudio.com/products/rstudio/download/#download

En mi caso me descargo el .deb: https://download1.rstudio.org/rstudio-xenial-1.1.463-amd64.deb

Para que no de [problemas](https://stackoverflow.com/a/20924082) la instalación de la libreria **devtools** instalamos primero:

```shell
$ sudo apt-get -y build-dep libcurl4-gnutls-dev
$ sudo apt-get -y install libcurl4-gnutls-dev
```

$ rstudio

En la consola de R studio
```
> install.packages("devtools")
> devtools::install_github("hmeleiro/elecciones")
```
